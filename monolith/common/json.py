from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
        #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
        #    return super().default(o)
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            props = {}
            if hasattr(o, "get_api_url"):
                props["href"] = o.get_api_url()
            for prop in self.properties:
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop]
                    value = encoder.default(value)
                props[prop] = value
            props.update(self.get_extra_data(o))
            return props
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
